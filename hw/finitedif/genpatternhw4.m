function [ pattern ] = genpatternhw4( size, alpha )
pattern = zeros(size,size);
pattern(1,1) = 1;
pattern(size,size) = 1;
p = [-alpha (1+2*alpha) -alpha];

for i = 2:size-1
    for j = i-1:i+1
        pattern(i,j) = p(i-j+2);
    end
end
end

