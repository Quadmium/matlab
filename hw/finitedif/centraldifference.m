function [centraldif] = centraldifference(funcvalues, step, shouldplot)
size = length(funcvalues);
difmat = gensecondcentral(size);
centraldif = 1/(2*step) * difmat * funcvalues;
if shouldplot
    f = figure('Position', [100, 100, 800, 300]);
    movegui(f,'center');
    plot(2:size-1, funcvalues(2:size-1), '', 2:size-1, centraldif(2:size-1), '');
    title('f(x) and central difference')
    ylabel('Y')
    xlabel('X')
    legend('f(x)', 'cd(x)')
end
end