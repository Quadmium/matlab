step = 1;
deltaT = 0.016;
% K * dt < 1/4?
[x,y,z]=meshgrid(-10:step:10);
x=x(:);
y=y(:);
z=z(:);
size=length(-10:step:10);
uN = zeros(size,size,size);
uN(2:size-1,2:size-1,2:size-1)=1;

%uN = 1./(.2*(x.^2+y.^2).^0.2+1);

% Circles
%uN = 1/2*abs(sin(x)+sin(y));
%uN = 1/2*abs(sin(0.4*x)+sin(0.4*y));

% | layout
%uN = zeros(length(x),length(y));
%uN(2:length(x)-1,2:10)=1;

%{
% [ layout
uN = zeros(length(x),length(y));
uN(2:length(x)-1,2:12)=1;
uN(2:12,2:length(x)-1)=1;
uN(length(x)-11:length(x)-1,2:length(x)-1)=1;
%}

%{
% Plus sign
uN = zeros(length(x),length(y));
uN(length(y)/2-5:length(y)/2+5,2:length(x)-1)=1;
uN(2:length(y)-1,length(x)/2-5:length(x)/2+5)=1;
%}

figure;
s = scatter3(x, y, z, 30, uN(:));
axis([-15 15 -15 15 -15 15]);
grid off;
colormap hot;
set(gca, 'CLim', [0, 1]);
for i=1:24000
    %centralder = dermaty * uN + uN * dermatx';
    c = deltaT*1/step^2;
    for j=2:size-1
       for k=2:size-1
           for l=2:size-1
               uN(j,k,l) = uN(j,k,l) + c * (uN(j-1,k,l) + uN(j+1,k,l) + uN(j,k-1,l) + uN(j,k+1,l) + uN(j,k,l-1) + uN(j,k,l+1) - 6*uN(j,k,l));
           end
       end
    end
    
    %uN = uN + deltaT * centralder;
    uN(1,:,:)=0;
    uN(:,1,:)=0;
    uN(:,:,1)=0;
    uN(end,:,:)=0;
    uN(:,end,:)=0;
    uN(:,:,end)=0;
    %uN(2:4,2:length(y)-1)=1;
    s.CData = uN(:);
    pause(0.01);
end