function [err] = hw4driver(N, tf, pl)
    b = @(x,t) sin(pi*x)*(exp(t)*(1+pi^2)-pi^2);
    xval = linspace(0,1,N);
    uE = sin(pi*xval)*(exp(tf)-1);
    stepS = xval(2)-xval(1);
    size = length(xval);
    uN = zeros(1,size);
    if pl
        p = plot(xval,uN, '--ro');
        hold on;
        axis([0 1 0 10]);
    end
    dt = 1/5 * stepS^2;
    mat = genpattern(length(uN));
    for t=0:dt:tf
        uN = uN + dt*(1/stepS^2*(mat*uN')' + b(xval,t));
        uN(1)=0;
        uN(length(uN))=0;
        if pl
            p.YData = uN;
            pause(.01 / N);
        end
    end
    if pl
        plot(xval,uE);
    end
    err = 1/sqrt(N) * norm(uN-uE);
end