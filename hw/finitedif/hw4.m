nval = 10:10:120;
err = zeros(1, length(nval));
driver=2;
for i=1:length(nval)
    if driver==1
        err(i) = hw4driver(nval(i), 1, false);
    else
        err(i) = hw4driver2(nval(i), 1, false);
    end
    fprintf('Calculated %d out of %d\n', i, length(nval));
end
stepval = 1./(nval-1);
loglog(stepval,err);
% Rough slope assuming log(y) and log(x)
fprintf('Slope: %d\n', (log(err(length(err)))-log(err(1)))/(log(stepval(length(stepval)))-log(stepval(1))));