step = 0.1;
deltaT = 0.0001;
xval = 0:step:10;
dermat = 1/step^2 * genpattern(length(xval));
uN = sin(2*pi*xval)' + cos(pi*xval)';
figure;
p = plot(xval, uN);
axis([0 10 -2 2]);
for i=1:6000
    centralder = dermat * uN;
    centralder(1,1)=0;
    centralder(end,end)=0;
    uN = uN + deltaT * centralder;
    p.YData = uN;
    pause(0.001);
end