legendinfo = {'f''(x)'};
f = figure('Position', [100, 100, 800, 300]);
movegui(f,'center');
endpt = 20;
exactsolution = exactf(0:0.1:endpt);
plot(0.1:0.1:endpt-0.1, exactsolution(2:end-1), '');
hold on;
ctr = 2;
for step = 0.2:0.4:4
xval = 0:step:20;
centraldif = centraldifference((xval.^5 + exp(xval))',step,false);
size = length(centraldif);
plot(step:step:endpt-step, centraldif(2:end-1), '');
legendinfo{ctr} = num2str(step);
ctr = ctr + 1;
end

title('f''(x) and central difference')
ylabel('Y')
xlabel('X')
legend(legendinfo);
