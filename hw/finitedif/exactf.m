function [exactsolution] = exactf(xval)
syms x;
xp = diff(x^5 + exp(x),x);
exactsolution = vpa(subs(xp,x,xval));
end