[X,Y] = meshgrid(-10:.1:10, -10:.1:10);
R = sqrt(X.^2 + Y.^2);
Z = sin(R)./R;
f = figure('Position', [100, 100, 800, 300]);
movegui(f,'center');
s = surf(X,Y,Z,'FaceColor','red','EdgeColor','none');
camlight left;
lighting phong;
for i=20:250
    Z = sin(200*R/i)./(200*R/i);
    s.ZData = Z;
    pause(0.05)
end
