function [mat] = gensecondcentral(size)
mat = zeros(size,size);
mat(1,1) = 1;
mat(size,size) = 1;

for i=2:size-1
    for j=i-1:i+1
        mat(i,j) = j-i;
    end
end

end