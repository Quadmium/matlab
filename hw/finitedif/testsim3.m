step = 0.4;
deltaT = 0.016;
% K * dt < 1/4?
[x,y] = meshgrid(-10:step:10, -10:step:10);
dermatx = 1/step^2 * genpattern(length(x));
dermaty = 1/step^2 * genpattern(length(y));

%uN = 1./(.2*(x.^2+y.^2).^0.2+1);

% Circles
%uN = 1/2*abs(sin(x)+sin(y));
uN = 1/2*abs(sin(0.4*x)+sin(0.4*y));

% | layout
%uN = zeros(length(x),length(y));
%uN(2:length(x)-1,2:10)=1;

%{
% [ layout
uN = zeros(length(x),length(y));
uN(2:length(x)-1,2:12)=1;
uN(2:12,2:length(x)-1)=1;
uN(length(x)-11:length(x)-1,2:length(x)-1)=1;
%}

%{
% Plus sign
uN = zeros(length(x),length(y));
uN(length(y)/2-5:length(y)/2+5,2:length(x)-1)=1;
uN(2:length(y)-1,length(x)/2-5:length(x)/2+5)=1;
%}

figure;
s = surf(x, y, uN, 'EdgeColor', 'None', 'facecolor', 'interp');
%s = surf(x, y, uN, 'EdgeColor', 'None');
axis([-10 10 -10 10 0 1]);
view(2);
grid off;
colormap hot;
set(gca, 'CLim', [0, 1]);
for i=1:24000
    %centralder = dermaty * uN + uN * dermatx';
    c = deltaT*1/step^2;
    for j=2:length(x)-1
       for k=2:length(y)-1
           uN(j,k) = uN(j,k) + c * (uN(j-1,k) + uN(j+1,k) + uN(j,k-1) + uN(j,k+1) - 4*uN(j,k));
       end
    end
    
    %uN = uN + deltaT * centralder;
    uN(1,:)=0;
    uN(:,1)=0;
    uN(end,:)=0;
    uN(:,end)=0;
    %uN(2:4,2:length(y)-1)=1;
    s.ZData = uN;
    pause(0.01);
end