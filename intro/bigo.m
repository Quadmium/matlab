n = 0:2.5:500;
f = figure('Position', [100, 100, 800, 300]);
movegui(f,'center')
subplot(1,2,1);
plot(n,n.*log(n),'b',n,sqrt(n),'r',n,log(n),'g')
title('Big-O characteristics: Linear Plot')
ylabel('Estimate of Running Time')
xlabel('n (number of elements)')
legend('O(n ln n)','O(sqrt n)','O(ln n)')
grid on;
subplot(1,2,2);
semilogy(n,n.*log(n),'b',n,sqrt(n),'r',n,log(n),'g')
title('Big-O characteristics: Logarithmic Plot')
ylabel('Estimate of Running Time')
xlabel('n (number of elements)')
legend('O(n ln n)','O(sqrt n)','O(ln n)')
grid off;