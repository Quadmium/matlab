g = 9.8;
v0 = 50.75;
y0 = 0;
x0 = 0;
theta = 5*pi/12;
deltas = [0 -0.255 -0.425];
f = figure('Position', [100, 100, 1000, 300]);
movegui(f,'center')
for i = 1:3
    theta0 = theta + deltas(i);
    t = 0 : 0.1 : 11-i;
    y = y0 - 0.5 * g * t.^2 + v0*sin(theta0).*t;
    x = x0 + v0*cos(theta0).*t;
    subplot(1,3,i);
    plot(x,y);
    title(['y vs. x (\Delta\theta = ',num2str(deltas(i)),')']);
    xlabel('Horizontal Distance (m)');
    ylabel('Altitude (m)');
    grid on;
end