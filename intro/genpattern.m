function [ pattern ] = genpattern( size )
%genpattern Generates a 1,-2,1 pattern
%   (1,1) = 1
%   (size,size) = 1
%   Between is alternating 1,-2,1 displaced by one column per row

% allocate memory
pattern = zeros(size,size);

% set extraneous points (1,1) and (size,size)
pattern(1,1) = 1;
pattern(size,size) = 1;

% loop through remaining points
% note: i-j becomes 1, 0, -1, and 3x^2-2 maps
%       those values to 1, -2, 1
for i = 2:size-1
    for j = i-1:i+1
        pattern(i,j) = 3*(i-j)^2 - 2;
    end
end
end

